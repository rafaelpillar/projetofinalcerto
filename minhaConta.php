<?php
// Template name:minhaConta 
 get_header(); ?>
<?php wp_nav_menu([
                    'menu'=>'Menu-Account',
                    'container'=>'nav',
                    'container_class'=>'contaMenu',
                ])
?>

<div class = "formConta">
	<div class="TextConta">
	<?php 
	do_action('woocommerce_account_content');
	?>
	</div>
	<form action="" method="post" >
		<div class="Nome-sobrenome">
			<div class="form-nome">
				<label for="nome" class="">Nome</label>
				<input type="text" id ="nome" class="" placeholder =  "Digite  seu nome">
			</div>
			<div class="form-sobrenome">
				<label for="sobrenome">Sobrenome</label>
				<input type="text" id="sobrenome" placeholder =  "Digite  seu Sobrenome">
			</div>
		</div>
		<div class="email">
				<label for="email">Email</label>
				<input type="email" id="email" placeholder =  "Digite  seu email">
        </div>
		<div class="senha">
				<label for="senha">Senha atual(deix em branco para não alterar)</label>
				<input type="password" id="senha" placeholder =  "Digite  sua senha atual">
		</div>
		<div class="novaSenha">
				<label for="nova_senha">Nova senha(deix em branco para não alterar)</label>
				<input type="password" id="nova_senha" placeholder =  "Digite  sua Nova senha">
		</div>
		<div class="nova_senhaConf">
				<label for="confirm_novaSenha">Confirmar nova senha</label>
				<input type="password" id="confirm_novaSenha" placeholder =  "Confirme sua nova senha">
		</div>
		<div class="btnForm">
			<a>Salvar alterações</a>
		</div>
	</form>
</div>

    
<?php get_footer(); ?>