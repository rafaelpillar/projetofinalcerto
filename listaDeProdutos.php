<?php
// Template name:lista de produtos 
 get_header(); ?>
<div class="contentListProducts">
    <h1>selecione uma categoria</h1>
    <div class="categoriasProducts">     
    <?php 
                $categorias_final = get_link_category_img();
                foreach($categorias_final as $category){
                    if($category['name'] != 'Uncategorized'){ ?>
                        <a href="<?php echo $category['link'] ?>">
                        <div class="categoriasListProductsImg" style="background-image:url('<?php echo $category['img']; ?>')" >
                            <div class="categoriasListProductsLink"><?php echo $category['name'] ?></div>
                        </div>
                        </a>
                    <?php
                    }
                };

                ?>  
    </div>
    <div class= "conteinerListText">
        <p class="pratosList">Pratos</p>
        <p class="foodList">comida japonesa</p>
    </div>        
    
    
<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
	<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x( 'Search', 'submit button', 'woocommerce' ); ?></button>
	<input type="hidden" name="post_type" value="product" />
</form>







<?php do_action( 'woocommerce_widget_price_filter_start', $args ); ?>

<form method="get" action="<?php echo esc_url( $form_action ); ?>">
	<div class="price_slider_wrapper">
		<div class="price_slider" style="display:none;"></div>
		<div class="price_slider_amount" data-step="<?php echo esc_attr( $step ); ?>">
			<input type="text" id="min_price" name="min_price" value="<?php echo esc_attr( $current_min_price ); ?>" data-min="<?php echo esc_attr( $min_price ); ?>" placeholder="<?php echo esc_attr__( 'Min price', 'woocommerce' ); ?>" />
			<input type="text" id="max_price" name="max_price" value="<?php echo esc_attr( $current_max_price ); ?>" data-max="<?php echo esc_attr( $max_price ); ?>" placeholder="<?php echo esc_attr__( 'Max price', 'woocommerce' ); ?>" />
			<?php /* translators: Filter: verb "to filter" */ ?>
			<button type="submit" class="button"><?php echo esc_html__( 'Filter', 'woocommerce' ); ?></button>
			<div class="price_label" style="display:none;">
				<?php echo esc_html__( 'Price:', 'woocommerce' ); ?> <span class="from"></span> &mdash; <span class="to"></span>
			</div>
			<?php echo wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true ); ?>
			<div class="clear"></div>
		</div>
	</div>
</form>

<?php do_action( 'woocommerce_widget_price_filter_end', $args ); ?>


    

    <section class="allProducts">
        <?php
        $args=[''];
        $products = wc_get_products($args);
        $product_formatado = format_products($products);
        foreach($product_formatado as $product) { ?>
            <div class="img-Produtos" style="background-image:url('<?php echo $product['img_url'] ?>')">
                <div class="container-infos-lp">
                    <p class="nome-Produtos"><?php echo $product['name']; ?></p>
                    <p class="Preco-Produtos">R$<?php echo $product['price']; ?></p>
                    <a href="<?php echo $product['link_prod'] ?>" class="adicionar-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/CarrinhoBtn.png" alt=""></a>
                </div>
            </div>
            <?php }; ?>
    </section>

</div>
    


 <?php get_footer(); ?>